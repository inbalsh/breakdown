<?php

use yii\db\Migration;

/**
 * Class m180622_193603_init_rbac
 */
class m180622_193603_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
  
        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $teamleader = $auth->createRole('teamleader');
        $auth->add($teamleader);

        $member = $auth->createRole('member');
        $auth->add($member);

        $manager = $auth->createRole('manager');
        $auth->add($manager);


        $manageUsers = $auth->createPermission('manageUsers');
        $auth->add($manageUsers);
        
        $manageBreakdown = $auth->createPermission('manageBreakdown');
        $auth->add($manageBreakdown); // ליצור לעדכן לצפות בתקלות

        $changeLevel = $auth->createPermission('changeLevel');
        $auth->add($changeLevel);   // לשנות לבל של תקלה                 
        
        $changeStatus = $auth->createPermission('changeStatus');
        $auth->add($changeStatus);                    

        $viewUrgent = $auth->createPermission('viewUrgent');
        $auth->add($viewUrgent);                    
        
     //   $viewUsers = $auth->createPermission('viewUsers');
     //   $auth->add($viewUsers);    
  
     //   $rule = new \app\rbac\AuthorRule;
      //  $auth->add($rule);
                
      //  $viewOwnUser->ruleName = $rule->name;                
      //  $auth->add($viewOwnUser);                 
                                 
        $auth->addChild($admin, $teamleader);
        $auth->addChild($teamleader, $member);      
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($member, $manageBreakdown); 
        $auth->addChild($teamleader, $changeLevel); 
        $auth->addChild($teamleader, $changeStatus); 
     //   $auth->addChild($updateOwnPost, $updatePost);
        $auth->addChild($manager, $viewUrgent);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180622_193603_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180622_193603_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
