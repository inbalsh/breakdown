<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m180622_184242_create_breakdown_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('breakdown', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'level' => $this->integer(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('breakdown');
    }
}
