<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $username
 * @property string $auth_key
 * @property string $password
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'username', 'auth_key', 'password'], 'string', 'max' => 255],
            [['username'], 'unique'],
            ['role', 'safe'], // הוספת שדה תפקיד !!!!!!        
        ];
    }


 // כל הפונקציות הבאות-מטרתן לקשר בין החלקות- שלנו ושל YII
 
 public static function findIdentity($id)
 {
     return static::findOne($id); // אם אנחנו מחוברים ורוצים לקבל עוד פרטים על היוזר
 }

 public static function findIdentityByAccessToken($token, $type = null)
 {
  //   return static::findOne(['access_token' => $token]);
 }

 public function getId()
 {
     return $this->id;
 }

 public function getAuthKey()
 {
     return $this->auth_key;
 }

 public function validateAuthKey($authKey)
 {
     return $this->auth_key === $authKey;
     // שלוש שווה- מאמת גם את סוג המשתנה, בדיקה יותר מחמירה
 }

 public static function findByUsername($username)
 {
     return self::findOne(['username'=>$username]);
     // בודקים במסד נתונים אם יש לנו יוזרניים כמו שקיבלנו
 }

  //צריך לממש ולידייט פסוורד
 // צריך לעשות משהו עם ההוסקי

 public function validatePassword($password){
    return \Yii::$app->security->validatePassword($password,$this->password);
 }


 public function beforeSave($insert){
     if(parent::beforeSave($insert)){
         if($this->isNewRecord){
             $this->auth_key = \Yii::$app->security->generateRandomString(); //פעולה המתבצעת פעם אחת כאשר יוצרים את היוזר
         }
         if($this->isAttributeChanged('password')){//מונע האש על האש
            $this->password = \Yii::$app->security->generatePasswordHash($this->password);
         }
         return true;
     }
     return false;
 }





    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password' => 'Password',
        ];
    }

    

// חלק של גל
        
//A method to get an array of all roles
    
   public static function getRoles()
   {

       $rolesObjects = Yii::$app->authManager->getRoles();
       $roles = [];
       foreach($rolesObjects as $id =>$rolObj){
           $roles[$id] = $rolObj->name; 
       }
       
       return $roles;    
         
   }   
}
