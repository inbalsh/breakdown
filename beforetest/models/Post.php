<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property string $category
 * @property int $author
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
    
        //השלוש שורות למטה זה כאשר רוצים לשנות את השם לקריאייטד ביי ואפדייטד ביי למשל. 
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['author', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'category', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }


        public function beforeSave($insert)
        {
            //preventing from users without editor or author permission
            //change from draft to publish
            if (parent::beforeSave($insert)) {
                if (\Yii::$app->user->can('editor')) {
                    return true;            
                } else {
                    if($this->status == 1){
                        return true;  
                    } else {
                        return false;     
                    }
                }
            }
            return false;
    }        
            

    public function getCategory1(){
        return $this->hasOne(Category::className(), ['id' => 'category']); // שינוי האיי די לשם הקטגוריה - בשדה קטגוריה
    }
    public function getStatus1(){
        return $this->hasOne(Status::className(), ['id' => 'status']); // שינוי האיי די לשם הקטגוריה - בשדה קטגוריה
    }
    public function getAuthor1()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }     

}
