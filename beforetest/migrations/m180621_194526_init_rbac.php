<?php

use yii\db\Migration;

/**
 * Class m180621_194526_init_rbac
 */
class m180621_194526_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $author = $auth->createRole('author');
        $auth->add($author);
  
        $admin = $auth->createRole('admin');
        $auth->add($admin);
      
        $editor = $auth->createRole('editor');
        $auth->add($editor);

        $auth->addChild($admin, $editor);
        $auth->addChild($editor, $author);
  
        $manageUsers = $auth->createPermission('manageUsers');
        $auth->add($manageUsers);
  
        $updatePost = $auth->createPermission('updatePost');
        $auth->add($updatePost);                    
        
        $publishPost = $auth->createPermission('publishPost');
        $auth->add($publishPost);                    
        
        $deletePost = $auth->createPermission('deletePost');
        $auth->add($deletePost);                    
        
        $createPost = $auth->createPermission('createPost');
        $auth->add($createPost);  

        $updateOwnPost = $auth->createPermission('updateOwnPost');
  
        $rule = new \app\rbac\AuthorRule;
        $auth->add($rule);
                
        $updateOwnPost->ruleName = $rule->name;                
        $auth->add($updateOwnPost);                 
                                  
                
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($editor, $updatePost); 
        $auth->addChild($editor, $publishPost); 
        $auth->addChild($editor, $deletePost); 
        $auth->addChild($updateOwnPost, $updatePost);
        $auth->addChild($author, $updateOwnPost);
        $auth->addChild($author, $createPost);

  
      
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_194526_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_194526_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
