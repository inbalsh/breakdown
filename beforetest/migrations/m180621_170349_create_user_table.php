<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180621_170349_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
            'username' => $this->string()->unique(),
            'auth_key' => $this->string(),
            'password' => $this->string(),        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
