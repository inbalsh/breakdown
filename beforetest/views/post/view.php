<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body:ntext',
           // 'category',
            [ // בשביל להציג בדף הצפייה של כל פוסט
                'label' => 'Category',
                'value' => $model->category1->category_name
            ],

          //  'status',
            [
                'label' => 'Status',
                'value' => $model->status1->status_name
            ],
            [    // הופך את שם הכותב לקישור שמפנה לדף ביוזרס                  
                'label' => 'Author',
				'format' => 'html',
				'value' => Html::a($model->author1->name, 
					['user/view', 'id' => $model->author1->id]),                
         //   'value' => $model->author1->name      השורה הזאת רק תציג בלי קישור
        ],

            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
